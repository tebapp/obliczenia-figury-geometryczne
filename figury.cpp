#include <iostream>

using namespace std;

void obliczeniaKwadrat();
void menu();

int main()
{
    int wybor=0;
    cout << "Obliczenia figur. ";
    while(1) {
        menu();
        cout << "Twoj wybor: ";
        cin >> wybor;
        if (wybor == 1) {
            cout << "Tutaj beda obliczenia dla trojkata\n";
        }
        else if (wybor == 2) {
            cout << "Tutaj beda obliczenia dla kola\n";
        }
        else if (wybor == 3) {
            obliczeniaKwadrat();
        }
        else if (wybor == 0) {
            break;
        }
        else {
            cout << "Dokonales zlego wyboru\n";
        }
        system("pause");
        system("cls");
    }
    cout << "Tutaj konczy sie nasz \"program\"\n";
    return 0;
}

void menu() {
    cout << "Wybierz jedna z opcji: " <<endl;
    cout << "1-trojkat" << endl;
    cout << "2-kolo" << endl;
    cout << "3-kwadrat" << endl;
    cout << "0- konczy program\n";
}

void obliczeniaKwadrat() {
    float a = .0f;
    system("cls");
    cout << "Dokonujemy obliczen dla kwadratu\n"
         << "Podaj bok kwadratu: ";
 cin >> a;
    cout << "Obwod kwadratu o boku a = "
         << a
         << " wynosi "
         << a*4
         << " a pole "
         << a*a
         << "\nNacisnij cokolwiek by prowcic do wyboru figury\n";
}
